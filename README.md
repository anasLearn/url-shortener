# SDA - Backend Technologies
## Exercise: URL shortener with Django

### Step 1:
Clone this repository in your local computer

### Step 2:
Install and run the virtual environment.

On Windows:
> Open a cmd or a PowerShell Terminal inside the folder
> 
Create a Python virtual environment, and activate it:
In Windows:
> python -m virtualenv venv
>
> venv\Scripts\activate

In Linux / Mac:
> python -m virtualenv venv
>
> source venv/bin/activate
> 

### Step 3:
Install the requirements in the virtual environment:

After the virtual environment is activated, run the following command:

> $ pip install -r requirements.txt


### Step 4:
Run the development server in your local computer:

> $ python manage.py runserver
