from django.http import HttpResponse, Http404
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic import CreateView

from .models import AliasedUrl
from .forms import AliasForm


def translator(request, alias):
    try:
        aliased = AliasedUrl.objects.get(short_url=alias)
        return redirect(aliased.original_url)

    except AliasedUrl.DoesNotExist:
        raise Http404("Alias not found")


class AliasCreateView(CreateView):
    form_class = AliasForm
    template_name = "form.html"
    # success_url = reverse_lazy("create_alias")

    def get_success_url(self):
        return f"{reverse_lazy('create_alias')}?created_alias={self.object.short_url}"
