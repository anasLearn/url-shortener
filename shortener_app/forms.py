from django.forms import ModelForm
from .models import AliasedUrl


class AliasForm(ModelForm):
    class Meta:
        model = AliasedUrl
        fields = "__all__"
