from django.db import models
from .utils import generate_aliases


class AliasedUrl(models.Model):
    short_url = models.CharField(unique=True, max_length=8, default=generate_aliases)
    original_url = models.URLField(blank=False)

    def __str__(self):
        # return f"Alias: {self.short_url}, Original_url: {self.original_url[:25]}..."
        return f"{self.short_url} -> {self.original_url}"
